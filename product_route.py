from fastapi import APIRouter
from product_api import *

router = APIRouter()

@router.get("/product/id")
async def find_product_by_id(params:dict):
    result = search_products_id(**params)
    return result
  
@router.get("/product/name")
async def find_prodcut_by_name(params:dict):
    result = search_product_by_name(**params)
    return result

@router.get("/products")
async def find_products():
    result = search_products()
    return result

@router.put("/product/update")
async def update_product(params:dict):
    return update_product(**params)

@router.delete("/product/delete")
async def delete_product_by_id(params:dict):
    return delete_product_by_id(**params)

@router.post("/product/insert")
async def instert_products(document:dict):
    return instert_products(**document)



    