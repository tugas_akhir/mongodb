from fastapi import FastAPI
from product_route import router as products_router

app = FastAPI()

app.include_router(products_router)

@app.get("/")
async def read_main():
    return {"message": "Hello Bigger Applications!"}

