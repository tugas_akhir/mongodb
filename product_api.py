from models.product_models import database as db
from bson import ObjectId

db = db()
obj = ObjectId()

def objIdToStr(obj):
    return str(obj["_id"])

def search_product_by_name(**params):
    data_list = []
    for product in db.searchProductByName(**params):
        product["_id"] = objIdToStr(product)
        data_list.append(product)
    return data_list

def search_products():
    data_list = []
    for product in db.showProducts():
        product["_id"] = objIdToStr(product)
        data_list.append(product)
    return data_list

def search_products_id(**params):
    result = db.showProductById(**params)
    result["_id"] = objIdToStr(result)
    return result

def insert_products(**document):
    db.insertProduct(**document)
    return {"massage":"Insert Succes"}

def update_product_id(**params):
    db.updateProductById(**params)
    return {"massage":"Update Succes"}
    
def delete_product_id(**params):
    db.deleteProductById(**params)
    return {"massage":"Delete Succes"}