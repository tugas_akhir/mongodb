from pymongo import MongoClient
from bson.objectid import ObjectId

class database:
    def __init__(self):
        try:
            self.nosql_db = MongoClient()
            self.db = self.nosql_db.no_sqldb
            self.mongo_prod = self.db.product
            print("database connected")
        except Exception as e:
            print(e)
    
    def showProducts(self):
        result = self.mongo_prod.find()
        return [item for item in result]
    
    def showProductById(self,**params):
        result = self.mongo_prod.find_one({"_id":ObjectId(params["id"])})
        return result
    
    def searchProductByName(self, **params):
        query = {"productname" : {"$regex": "{0}".format(params["productname"]), "$options": "i"}}
        result = self.mongo_prod.find(query)
        return result
    
    def insertProduct(self,**document):
        self.mongo_prod.insert_one(document)
        
    
    def updateProductById(self,**params):
        query_1 = {"_id":ObjectId(params["id"])}
        query_2 = {"$set": params["data"]}
        self.mongo_prod.update_one(query_1,query_2)
    
    def deleteProductById(self, **params):
        query = {"_id":ObjectId(params["id"])}
        self.mongo_prod.delete(query)